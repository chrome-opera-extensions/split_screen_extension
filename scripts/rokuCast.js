((w) => {
	'use strict';

	var _rokuCast = (item) => {
		var title = "Special Video";
		var rex = /\/([^\/]+)(\.3gp|\.avi|\.m4v|\.mkv|\.mp3|\.mp4|\.mpeg|\.mpg|\.webm|\.wmv|\.m3u8)/;
		if(rex.test(item)) {
			try {
				title = item.match(rex)[1];
			} catch (ex) {
				console.log({"TitleError": ex});
			}
		}
		const ip = "192.168.0.4";
		var video;
		var xhr = new XMLHttpRequest();
		xhr.open('HEAD', item, true);
		xhr.onload = function() {
			var video;
			console.log("xhrResponseURL:", xhr.responseURL);
			video = xhr.responseURL;
			if(/\?mime=true/.test(video)) {
				video = video.replace(/\?mime=true/g, '');
			}
			var mime = _MimeWorker.GetMimeType(item);
			var ext = (!mime) ? "mp4" : mime.ext;
			video = encodeURIComponent(video);
			title = encodeURIComponent(title);
			var url = `http://${ip}:8060/input/15985?t=v&u=${video}&videoName=${title}&k=(null)&videoFormat=${ext}`;
			console.log("casting_url:", url);
			var async = true;
			var request = new XMLHttpRequest();
			request.open("POST", url, async);
			request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			request.send(null);
		};
		xhr.send(null);
	}
	window.rokuCast = _rokuCast;
})(window, document);