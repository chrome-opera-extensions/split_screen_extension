(() => {
     function cancelFullScreen(el) {
          var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
          if(requestMethod) { // cancel full screen.
               requestMethod.call(el);
          }
     }

     function requestFullScreen(el) {
          // Supports most browsers and their versions.
          var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;

          if(requestMethod) { // Native full screen.
               requestMethod.call(el);
          }
          return false
     }

     function toggleFull() {
          var elem = document.body; // Make the body go full screen.
          var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);

          if(isInFullScreen) {
               cancelFullScreen(document);
          } else {
               requestFullScreen(elem);
          }
          return false;
     }

     var CELS = {
          el(tag, opts) {
               const el = document.createElement(tag);
               var attrNames = ["text", "textContent", "value", "data", "code"]
               for(let attr in opts) {
                    if(!attrNames.includes(attr) && attr !== "style") {
                         //if(attr !== "style") {
                         el.setAttribute(attr, opts[attr]);
                         //} else {
                         //el.setAttribute(attr, opts[attr].cssText);
                         //     el[attr].cssText = opts[attr].cssText;
                         //}
                    } else {
                         Object.assign(el, {
                              [attr]: opts[attr]
                         });
                    }
               }
               return el
          },
          h(name, attributes) {
               var rest = [];
               var children = [];
               var el = this.el(name, attributes);
               var length = arguments.length;
               while(length-- > 2) rest.push(arguments[length]);
               while(rest.length) {
                    var node = rest.pop();
                    if(node && node.pop) {
                         for(length = node.length; length--;) {
                              rest.push(node[length]);
                         }
                    } else if(node != null && node !== true && node !== false) {
                         el.append(node);
                         children.push(node);
                    }
               }
               return el;
          }
     }

     var setupSplit = () => {
          /*
          var CreateElement = function(vid, id) {
          	var mime = _MimeWorker.GetMimeType(vid);
               //console.log(mime);
               var div = document.createElement("div");
          	if(!mime) {
                    div.innerHTML = `<iframe id="vidframe${id}", class="vidframe" src="${vid}" scrolling="no" frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" align="middle"></iframe>`;
                    return div;
          	}
               div.innerHTML = `<video id="split-cast-vid${id}" height="100%" width="100%" name="media" controls preload><source src="${vid}" type="${mime.type}"></video>`;
               return div;
          }
          */
          var CreateElement = function(vid, id) {
               var mime = _MimeWorker.GetMimeType(vid);
               //console.log(mime);
               /*
               if(!mime) {
                    var returnElem = CELS.el("iframe", {
                         id: `vidframe${id}`,
                         class: "vidframe",
                         src: vid,
                         scrolling: "no",
                         frameborder: "0",
                         allowfullscreen: "true",
                         webkitallowfullscreen: "true",
                         mozallowfullscreen: "true",
                         align: "middle",
                         style: "width: 100%; height: 100%;"
                    });
                    return returnElem;
               }
               */
               var vidElem = CELS.h("video", {
                    id: `split-cast-vid${id}`,
                    "data-setup": '{ "playbackRates": [0.25, 0.5, 1, 1.5, 2] }',
                    class: "vjs-tech video-js vjs-default-skin",
                    type: mime.type,
                    height: "100%",
                    width: "100%",
                    name: "media",
                    controls: true,
                    preload: "auto",
                    "vjs-video": true,
                    style: "width: 100%; height: 100%;"
               }, [
                    CELS.h("source", {
                         src: vid,
                         type: mime.type //"video/mp4"
                    }, [
                         CELS.h("p", {
                              class: "vjs-no-js",
                              textContent: "To view this video please enable JavaScript, and consider upgrading to a web browser that"
                         }),
                         CELS.h("br", {}),
                         CELS.h("a", {
                              href: "http://videojs.com/html5-video-support/",
                              target: "_blank",
                              textContent: "supports HTML5 video"
                         })
                    ])
               ]);
               if(mime.type === "application/x-mpegURL") {
                    let hls = new Hls();
                    hls.loadSource(vid);
                    hls.attachMedia(vidElem);
                    hls.on(Hls.Events.MANIFEST_PARSED, function() {
                         video.play();
                    });
               }
               return vidElem;

               var video = document.createElement("video");
               video.id = "split-cast-vid"
               video.setAttribute("data-setup", '{ "playbackRates": [0.25, 0.5, 1, 1.5, 2] }');
               video.classList = "vjs-tech video-js vjs-default-skin";
               var div = document.createElement("div");
               var videoElem = `<video id="split-cast-vid${id}" preload="auto" `;
               videoElem += `data-setup='{ "playbackRates": [0.25, 0.5, 1, 1.5, 2] }' `;
               videoElem += `controls class="vjs-tech video-js vjs-default-skin" vjs-video`;
               videoElem += ` type="${mime.type}" height="100%" width="100%" name="media">`;
               videoElem += `<source src="${vid}" type='video/mp4'><p class="vjs-no-js">`;
               videoElem += `To view this video please enable JavaScript, and consider upgrading to a web browser that<br>`;
               videoElem += `<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>`;
               videoElem += `</p></video>`;
               div.innerHTML = videoElem;
               console.log("div:", div);
               videoElem = div.firstChild;
               return videoElem;
          }
          var fullbutton = document.createElement('input');
          fullbutton.type = 'submit';
          fullbutton.value = 'Full Screen';
          fullbutton.id = '_fullbutton';
          fullbutton.onclick = function() {
               if(this.value === "Full Screen") {
                    this.value = "Exit Full Screen";
               } else {
                    this.value = "Full Screen";
               }
               toggleFull();
          }
          var button = document.createElement('input');
          button.type = 'submit';
          button.value = 'Split Screens x4';
          button.id = '_split';
          button.onclick = function() {
               var createElements = function(i) {
                    var createElementFromHTML = function(htmlString) {
                         var div = document.createElement('div');
                         div.innerHTML = htmlString.trim();
                         return div.firstChild;
                    }
                    var textBox = createElementFromHTML(`<input type="text" id="_input${i}">`);
                    var openVid = createElementFromHTML(`<input type="submit" value="Open file" id="_openvid${i}">`);
                    var embedVid = createElementFromHTML(`<input type="submit" value="Submit" id="_embedvid${i}">`);

                    openVid.onclick = function() {
                         document.getElementById('content').code = `_input${i}`;
                         document.getElementById('addvideo').click();
                    }

                    embedVid.onclick = function() {
                         var vid = document.getElementById(`_input${i}`).value;
                         var p = CELS.el("p", {
                              id: `_p${i}`,
                              align: "middle",
                              style: "width: 100%; height: 100%;"
                         });
                         var elem; // = CreateElement(vid);
                         if(!!document.querySelector(`#_div${i} #_p${i}`)) {
                              p = document.querySelector(`#_div${i} #_p${i}`);
                              elem = p.querySelector('iframe');
                              if(!!p.childNodes[0].childNodes[0]) {
                                   elem = p.childNodes[0];
                                   //elem.childNodes[0].src = vid;
                                   elem.childNodes[0].replaceWith(CreateElement(vid));
                              } else {
                                   elem = p.childNodes[0];
                                   //elem.src = vid;
                                   elem.replaceWith(CreateElement(vid));
                              }
                              p.appendChild(elem);
                         } else {
                              //p.innerHTML = elem;
                              var VElem = CreateElement(vid, i);
                              p.appendChild(VElem);
                              setTimeout(() => {
                                   window.myPlayer = videojs(VElem.id);
                              }, 0);
                         }
                         div.appendChild(p);
                         document.getElementById(`_div${i}control`).style.display = 'none';
                         p.style.height = '100%';
                         document.getElementById(`_div${i}`).addEventListener('mouseover', function() {
                              document.getElementById(`_div${i}control`).style.display = 'block';
                              document.getElementById(`_p${i}`).style.height = 'calc(100% - 22px)';
                         });
                         document.getElementById(`_div${i}`).addEventListener('mouseout', function() {
                              document.getElementById(`_div${i}control`).style.display = 'none';
                              document.getElementById(`_p${i}`).style.height = '100%';
                         });
                    }

                    var castVid = document.createElement('input');
                    castVid.type = "submit";
                    castVid.value = "Cast";
                    castVid.id = `_cast${i}`;
                    castVid.onclick = function() {
                         var vid = document.getElementById(`_input${i}`);
                         vid = vid.value;
                         //console.log(vid);
                         rokuCast(vid);
                    }

                    var divControl = document.createElement('div');
                    divControl.id = `_div${i}control`;
                    divControl.appendChild(textBox);
                    divControl.appendChild(openVid);
                    divControl.appendChild(embedVid);
                    divControl.appendChild(castVid);

                    var div = document.createElement('div');
                    div.id = `_div${i}`;
                    div.appendChild(divControl);
                    document.getElementById('content').appendChild(div);
               }
               if(!!document.getElementById('_div1') && !!document.getElementById('_div2') && !!document.getElementById('_div3') && !!document.getElementById('_div4')) {
                    if(confirm('Clear Screens?')) {
                         location.href = location.href.split('?')[0];
                         //document.getElementById('_div1').remove();
                         //document.getElementById('_div2').remove();
                         //document.getElementById('_div4').remove();
                         //document.getElementById('_div3').remove();
                         //this.click();
                         //this.value = 'Split x4';
                    }
               } else if(!!document.getElementById('_div1') && !!document.getElementById('_div2')) {
                    this.value = "Clear Split Screens";
                    var cssText = `#_div1,#_div2,#_div3,#_div4{width:calc(50% - 2px);height:calc(50% - 2px);background-color:#fff;float:left;margin:0;padding:0;border:1px solid #000}.vidframe,p{margin:auto}#_input1,#_input2,#_input3,#_input4{position:relative;top:0;left:0;z-index:1000;width:65%}p{height:95%}.vidframe{width:100%;height:100%}`
                    var cssTextNode = document.createTextNode(cssText);
                    var style = document.createElement('style');
                    style.appendChild(cssTextNode);
                    style.id = "_css2";
                    document.getElementById('_css1').remove();
                    document.head.appendChild(style);
                    createElements(3);
                    createElements(4);
               } else {
                    var cssText = `#_div1,#_div2{width:50%;height:100%;float:left;margin:0;padding:0;border:0}#_div1{background-color:#000}#_div2{background-color:#ff0}.vidframe,p{margin:auto}#_input1,#_input2{position:relative;top:0;left:0;z-index:1000;width:65%}p{height:95%}.vidframe{width:100%;height:100%}`;
                    var cssTextNode = document.createTextNode(cssText);
                    var style = document.createElement('style');
                    style.id = "_css1";
                    style.appendChild(cssTextNode);
                    document.head.appendChild(style);
                    createElements(1);
                    createElements(2);
               }
          }
          var toprightcontrol = document.createElement('div');
          toprightcontrol.id = '_toprightcontrol';
          toprightcontrol.appendChild(fullbutton);
          toprightcontrol.appendChild(button);
          document.getElementById('content').appendChild(toprightcontrol);
          document.getElementById('_toprightcontrol').style.display = 'none';
          document.getElementById('content').addEventListener('mouseover', function() {
               document.getElementById('_toprightcontrol').style.display = 'block';
          });
          document.getElementById('content').addEventListener('mouseout', function() {
               document.getElementById('_toprightcontrol').style.display = 'none';
          });
          button.click();
     }
     var getQueryString = () => {
          return new Promise((resolve) => {
               var m = [];
               location.search.split('url=')
                    .forEach((e, i) => {
                         e = e.replace(/&$/g, '');
                         if(e !== '?') {
                              var r = e.replace(/&$/g, ''); //e.replace('&', '');
                              //console.log(r);
                              m.push(r);
                         }
                         if(i === (location.search.split('url=').length - 1)) {
                              resolve(m);
                         }
                    });
          });
     }
     window.getQueryString = getQueryString;
     document.getElementById('content').code = [];
     window.onload = function() {
          setupSplit();
          if(!!location.search != (null || "")) {
               getQueryString().then((urls) => {
                    if(urls.length > 2) {
                         document.querySelector('#_split').click();
                         urls.forEach((url, i) => {
                              if(i < 4) {
                                   var texts = document.querySelectorAll('input[type="text"]');
                                   texts[i].value = url;
                              }
                         });
                    } else {
                         urls.forEach((url, i) => {
                              var texts = document.querySelectorAll('input[type="text"]');
                              texts[i].value = url;
                         });
                    }
               });
          }
     }
})();
var handleVideo = function(evt) {
     var fileURL = URL.createObjectURL(evt.target.files[0]);
     document.getElementById(document.getElementById('content').code).value = fileURL;
}
document.getElementById('addvideo').addEventListener('change', handleVideo, false);