((w) => {
     'use strict';

     var _MimeWorker = {
          MimeTypes: {
               mp4: {
                    type: "video/mp4",
                    ext: "mp4"
               },
               mp3: {
                    tyle: "audio/mpeg",
                    ext: "mp3"
               },
               webm: {
                    type: "video/webm",
                    ext: "webm"
               },
               m4v: {
                    type: "video/m4v",
                    ext: "m4v"
               },
               mkv: {
                    type: "video/x-matroska",
                    ext: "mkv"
               },
               avi: {
                    type: "video/x-msvideo",
                    ext: "avi"
               },
               wmv: {
                    type: "video/x-ms-wmv",
                    ext: "wmv"
               },
               _3gp: {
                    type: "video/3gpp",
                    ext: "3gp"
               },
               m3u8: {
                    type: "application/x-mpegURL",
                    ext: "hls"
               }
          },
          GetMimeType: function(item) {
               if(/youtube|openload/.test(item)) {
                    if(/mp4|webm|3gp/.test(item)) {
                         var ext = /mp4|webm|3gp|m3u8/.exec(item)[0];
                         item = item.replace(ext, "." + ext);
                    } else {
                         return _MimeWorker.MimeTypes.mp4;
                    }
               }
               var extension = /\.3gp|\.avi|\.divx|\.flv|\.m4v|\.mkv|\.mp3|\.mp4|\.mpeg|\.mpg|\.webm|\.wmv|\.m3u8/.exec(item);
               if(extension === null) return false;
               switch (extension[0]) {
                    case ".mp4":
                         return _MimeWorker.MimeTypes.mp4;
                    case ".mp3":
                         return _MimeWorker.MimeTypes.mp3;
                    case ".webm":
                         return _MimeWorker.MimeTypes.webm;
                    case ".m4v":
                         return _MimeWorker.MimeTypes.m4v;
                    case ".avi":
                         return _MimeWorker.MimeTypes.avi;
                    case ".wmv":
                         return _MimeWorker.MimeTypes.wmv;
                    case ".3gp":
                         return _MimeWorker.MimeTypes._3gp;
                    case ".m3u8":
                         return _MimeWorker.MimeTypes.m3u8;
                    default:
                         return false;
               }
          }
     }
     w._MimeWorker = _MimeWorker;
})(window);