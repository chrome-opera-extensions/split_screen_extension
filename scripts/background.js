chrome.browserAction.onClicked.addListener(function() {
	chrome.tabs.create({
		url: "/html/splitscreen.html"
	});
});

var getFrames = () => {
	var iframes = document.querySelectorAll('iframe');
	var exports = {}
	iframes.forEach((iframe, i) => {
		if(iframe.src != null && iframe.src !== "") {
			exports[i] = iframe.src;
		}
	});
	var exp = JSON.stringify(exports);
	//console.log(exp);
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		window.tab = tabs[0];
	});
	//return exports;
}

var openSplitScreen = function() {
	/*
	    chrome.tabs.getSelected(null, function(tab) {
	        // Now inject a script onto the page
	        chrome.tabs.executeScript(tab.id, {
	            code: "chrome.extension.sendMessage({content: document.body.innerHTML}, function(test) { console.log('test'); });"
	        }, function() {
	            console.log('done');
	        });
	    
	    });
	*/
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		chrome.tabs.executeScript(tabs[0].id, {
			code: `
            var cssText = '#_overlay {'
			+ '  position: fixed !important;'
			+ '  display: none;'
			+ '  width: 100% !important;'
			+ '  height: 100% !important;'
			+ '  top: 0 !important;'
			+ '  left: 0 !important;'
			+ '  right: 0 !important;'
			+ '  bottom: 0 !important;'
			+ '  background-color: rgba(0,0,0,0.5) !important;'
			+ '  z-index: 200000 !important;'
			+ '}'
			+ '#_text {'
			+ '  position: absolute !important;'
			+ '  top: 50% !important;'
			+ '  left: 50% !important;'
			+ '  height: 80% !important;'
			+ '  width: 80% !important;'
			+ '  font-size: 14px !important;'
			+ '  font-family: Arial !important;'
			+ '  color: black !important;'
			+ '  background-color: white !important;'
			+ '  transform: translate(-50%,-50%) !important;'
			+ '  -ms-transform: translate(-50%,-50%) !important;'
			+ '}'
			+ '#_exit {'
			+ '  position: absolute !important;'
			+ '  top: 5px !important;'
			+ '  right: 5px !important;'
			+ '  z-index: 20000000 !important;'
			+ '  font-size: 25px !important;'
			+ '  font-weight: bold !important;'
			+ '  color: white !important;'
			+ '  cursor: pointer !important;'
            + '}'
            
			var cssTextNode = document.createTextNode(cssText);
			var css = document.createElement('style');
			css.appendChild(cssTextNode);
			css.id = 'findValues';
			document.head.appendChild(css);
			var exports = [];
			if(/openload/.test(location.href)) {
				var ovidname = location.pathname.replace(/(\\/f\\/|\\/embed\\/)|\\//g, '');
				var orex = new RegExp(ovidname);
				var oclickid;
				document.querySelectorAll('div[style] p').forEach(e => {
					if(orex.test(e.outerText)) {
						oclickid = e.id;
						var ovid = '/stream/' + document.getElementById(oclickid).innerText;
						var ovideo = location.protocol + '//' + location.host + ovid;
						exports.push(ovideo);
					}
				});
			}
            var iframes = document.querySelectorAll('iframe');
            iframes.forEach((iframe, i) => {
                if(iframe.src !== null && iframe.src !== "") {
                    exports.push(iframe.src);
                }
			});
			if(!!document.querySelector('video')) {
				if(!!document.querySelector('video').src) {
					exports.push(document.querySelector('video').src);
				}
				if(!!document.querySelector('video source')) {
					exports.push(document.querySelector('video source').src);
				}
			}
            //var exp = JSON.stringify(exports);
            var _overlay = document.createElement('div');
			_overlay.id = '_overlay';
			var _textarea = document.createElement('div');
			_textarea.id = '_text';
			_textarea.name = 'text';
            //if(exports === {}) imports.text = 'Nothing found';
            var ol = document.createElement('ol');
            exports.forEach((e, i) => {
                var li = document.createElement('li');
                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.id = 'checkbox_' + i;
                var span = document.createElement('span');
                span.id = '_span' + i;
                span.innerText = e;
                li.appendChild(checkbox);
                li.appendChild(document.createTextNode(' '));
                li.appendChild(span);
                //li.appendChild(document.createTextElement(' '));
                ol.appendChild(li);
                var url;
                //btn.onclick = function() {
                //    var id = btn.id.split('_')[1];
                //    url = btn.parentElement.querySelector('span').innerText
                //    chrome.extension.sendMessage({content: url}, function(test) { console.log('success'); });
                //}
                //_textarea.value += e + '\\n';
            });
            //var li = document.createElement('li');
            //ol.appendChild(li);
            var btn = document.createElement('input');
            btn.type = 'submit';
            btn.value = 'Split URLs';
            btn.onclick = function() {
                var url;
				var checkboxes = document.getElementById('_overlay').querySelectorAll('input[type="checkbox"]');
				checkboxes.forEach((checkbox) => {
					if(checkbox.checked) {
						url = (url === null) ?  url + '&url' + checkbox.parentElement.querySelector('span').innerText : checkbox.parentElement.querySelector('span').innerText;
					}
				});
                //checkboxes.forEach((checkbox,i) => {
                //    if(checkbox.checked) {
                //        if(i > 0) {
                //            url += '&url=' + checkbox.parentElement.querySelector('span').innerText;
                //        }
                //        else {
                //            url = checkbox.parentElement.querySelector('span').innerText;
                //        }
                //    }
                //});
                chrome.extension.sendMessage({content: url}, function(test) { console.log('success'); });
            }
            ol.appendChild(btn);
            _textarea.appendChild(ol);
            //_textarea.appendChild(document.createElement('br'));
            //_textarea.appendChild(btn);
			//_textarea.value = exp;
			_overlay.appendChild(_textarea);
			_overlay.style.display = 'block';
			var span = document.createElement('span');
			span.id = '_exit';
			span.innerHTML = '&#x2717;';
			span.addEventListener('click', () => {
				document.getElementById('_overlay').remove();
			});
            _overlay.appendChild(span);
            _overlay.onclick = function(evt) {
                if(evt.srcElement.id === '_overlay') {
                    document.getElementById('_overlay').remove();
                }
            }
            document.body.appendChild(_overlay);
            `
		}, (result) => {
			//console.log(result);
		});
	});

};

var splitLink = function(evt) {
	var m = _MimeWorker.GetMimeType(evt.linkUrl);
	//console.log(m);
	if(m) {
		chrome.tabs.query({
			"active": true,
			"lastFocusedWindow": true
		}, (tabs) => {
			chrome.tabs.executeScript(tabs[0].id, {
				code: `chrome.extension.sendMessage({content: '${evt.linkUrl}'}, function(test) { console.log('success'); });`
			}, (result) => {

			});
		});
	}
}

var castLink = function(evt) {
	var m = _MimeWorker.GetMimeType(evt.linkUrl);
	//console.log(m);
	/**
	 * evt elements are as follows
	 * evt {
	 *     editable: false,
	 *     frameId: 0,
	 *     linkUrl: "https://url.from.link"
	 *     menuItemId: createContextMenu
	 *     pageUrl: "https://url.path/"
	 * }
	 */
	if(m) {
		console.log(evt.linkUrl);
		rokuCast(evt.linkUrl);
	}
}

//chrome.contextMenus.onClicked.addListener(openSplitScreen);

var MAINMENUID = null;
var CASTLINKID = null;
var SPLITLINKID = null;
var createContextMenu = () => {
	var context = "page";
	var optionsNew = {
		title: "Find iFrames",
		contexts: [context],
		id: "context" + context,
		onclick: openSplitScreen
	}
	var optionsUpdate = {
		title: "Find iFrames",
		contexts: [context],
		onclick: openSplitScreen
	}
	var castOptions = {
		title: "Cast Link",
		contexts: ["link"],
		id: "contextCastlink",
		onclick: castLink
	}
	var castUpdate = {
		title: "Cast Link",
		contexts: ["link"],
		onclick: castLink
	}

	var splitOptions = {
		title: "Split Link",
		contexts: ["link"],
		id: "contextSplitlink",
		onclick: splitLink
	}

	var splitUpdate = {
		title: "Split Link",
		contexts: ["link"],
		onclick: splitLink
	}

	if(MAINMENUID !== undefined && MAINMENUID !== "undefined" && MAINMENUID !== null) {
		//console.log(JSON.stringify({CASTLINKID: CASTLINKID}));
		chrome.contextMenus.update(MAINMENUID, optionsUpdate);
	} else {
		MAINMENUID = chrome.contextMenus.create(optionsNew);
	}

	if(CASTLINKID !== undefined && CASTLINKID !== "undefined" && CASTLINKID !== null) {
		//console.log(JSON.stringify({CASTLINKID: CASTLINKID}));
		chrome.contextMenus.update(CASTLINKID, castUpdate);
	} else {
		CASTLINKID = chrome.contextMenus.create(castOptions);
	}

	if(SPLITLINKID !== undefined && SPLITLINKID !== "undefined" && SPLITLINKID !== null) {
		//console.log(JSON.stringify({SPLITLINKID: SPLITLINKID}))
		chrome.contextMenus.update(SPLITLINKID, splitUpdate);
	} else {
		SPLITLINKID = chrome.contextMenus.create(splitOptions);
	}
}

chrome.runtime.onInstalled.addListener((details) => {
	createContextMenu();
});

chrome.tabs.onCreated.addListener((e) => {
	if(chrome.extension.inIncognitoContext) {
		createContextMenu();
	}
	if(!chrome.extension.inIncognitoContext) {
		createContextMenu();
	}
});


chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		// LOG THE CONTENTS HERE
		if(request.content) {
			//console.log(request.content);
			var continuation = () => {
				return new Promise((resolve) => {
					var isSplitScreenOpen;
					chrome.tabs.query({}, (tabs) => {
						for(var i = 0; i < tabs.length; i++) {
							if(tabs[i].title === 'Split Screens') {
								isSplitScreenOpen = tabs[i];
								break;
								//tab.url = "/html/splitscreen.html?url=" + request.content;
							}
							isSplitScreenOpen = false;
						}
					});
					var cycle = setInterval(() => {
						if(isSplitScreenOpen != null) {
							clearInterval(cycle);
							resolve(isSplitScreenOpen);
						}
					}, 100);
				})
			}
			continuation().then((result) => {
				if(!result) {
					chrome.tabs.create({
						url: "/html/splitscreen.html?url=" + request.content
					});
				} else {;
					/*if(!!result.url.split('url=')) {
                        var a = document.createElement('a');
                        a.href = url;
                        var queryString = a.search;
                        queryString += ''
					}*/
					var qstr;
					if(/url=/.test(result.url)) {
						qstr = result.url.substr(result.url.indexOf('?')).replace('?', "") + '&url=' + request.content;
						//qstr = result.url.split('?')[1] + '&url=' + request.content;
					} else {
						qstr = 'url=' + request.content;
					}
					chrome.tabs.update(result.id, {
						url: "/html/splitscreen.html?" + qstr,
						active: true
					});
				}
			});
		};
	}
);